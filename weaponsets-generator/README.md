# weaponsets-generator

This is a PHP script that generates individual weaponset pages for the new Hugo-based documentation site.

It also creates a `weaponsets/all` page which has a table of _all_ weaponsets, as well as an attached .csv file (the attachment function requires the "Relearn" Hugo theme to work).

## Limitations:

* Currently, there is no escaping for anything that may break Dokuwiki's syntax. This is not an issue right now; if it becomes one I will revise the script.

* The script does not use friendly names for the weaponset "categories". This is OK for the category named "Default Sets", but not so much for "Arsenal_mhide".

* The script does not put the categories in any particular order; only the order in which it finds them, so you may need to spend a minute reorganizing the index page.
