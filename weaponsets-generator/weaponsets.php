#!/usr/bin/php
<?php

require_once 'resources/vdf.php';
require 'resources/aliases.php';

function help()
{
  global $argv;

  echo
    "Generates weapon set pages using GE:S loadout scripts.\n\n".

    "Usage: " . basename($argv[0]) . " \"path\"\n".
    "where \"path\" is the location of your loadouts folder.\n\n".

    "The wiki pages will be saved to <current working directory>/weaponsets/*\n";
}

function validateArgs()
{
  global $argv;

  if( isset($argv[1]) )
  {

    if( !file_exists($argv[1]) )
    {

      echo "The provided path could not be found:\n"
        .$argv[1]."\n\n";
      help();
      exit(1);

    }

  } else {

    echo "Missing arguments.\n\n";
    help();
    exit(1);

  }

  if( file_exists( './weapon_sets' ) )
  {

    if( @$argv[2] != '-f' ) // '@' ignores undefined offset error here
    {

      echo "Warning:\n".
        "The following destination directory or file exists and will be overwritten:\n\n".
        getcwd()."/weapon_sets/\n".
        "Use -f to force the execution.\n";
      exit(1);

    }

  }


  return(TRUE);
}

function generate($loadoutPath)
{
  echo "Searching for weapon set scripts inside {$loadoutPath}\n\n";

  $loadoutFiles = glob($loadoutPath . '/weapon_sets*');

  echo "Found the following weapon set scripts:\n";

  foreach ($loadoutFiles as $file) {
    echo $file . "\n";
  }

  echo "\n";

  if( !file_exists('./weaponsets') )
  {

    if( !mkdir('./weaponsets', 0770) )
    {

      exit("Error creating directory \'weaponsets\'\n");

    }

  }

  if( !file_exists('./weaponsets/all/index.files') )
  {

    if( !mkdir('./weaponsets/all/index.files', 0770, true) )
    {

      exit("Error creating directory 'weaponsets/all/index.files'\n");

    }

  }

  // create/open our summary page and CSV for writing
  $weapon_set_summary_page = fopen("./weaponsets/all/index.md", "w");
  $weapon_set_summary_csv  = fopen("./weaponsets/all/index.files/weaponsets.csv", "w");

  // Write the first line of the summary page (front matter, table header, etc)
  require 'resources/weaponset_summary_template.php';
  fwrite($weapon_set_summary_page, $summary_start);

  // Write the first line of the CSV file
  fputcsv($weapon_set_summary_csv, $csv_header);

  foreach ($loadoutFiles as $file) {

    // Loop through each loadout script and generate the pages
    echo "Processing $file\n";

    $data = file_get_contents($file);
    $data = vdf_decode($data);

    // Loop through top-level keys (category)
    foreach ($data as $category_key => $category_value) {
      echo "Found category $category_key\n";
      $category_key = str_replace(' ', '_', $category_key);

      // Loop through each weapon set
      foreach ($category_value as $weaponset => $weaponset_value) {
        echo "  Found weapon set $weaponset\n";

        $ws_category = $category_key;
        $ws_name = $weaponset_value['print_name'];
        $ws_weight = $weaponset_value['weight'];

        $ws_level_1 = NAMES[$weaponset_value['weapons']['0']];
        $ws_level_2 = NAMES[$weaponset_value['weapons']['1']];
        $ws_level_3 = NAMES[$weaponset_value['weapons']['2']];
        $ws_level_4 = NAMES[$weaponset_value['weapons']['3']];
        $ws_level_5 = NAMES[$weaponset_value['weapons']['4']];
        $ws_level_6 = NAMES[$weaponset_value['weapons']['5']];
        $ws_level_7 = NAMES[$weaponset_value['weapons']['6']];
        $ws_level_8 = NAMES[$weaponset_value['weapons']['7']];

        $ws_level_1_icon = ICONS[$weaponset_value['weapons']['0']];
        $ws_level_2_icon = ICONS[$weaponset_value['weapons']['1']];
        $ws_level_3_icon = ICONS[$weaponset_value['weapons']['2']];
        $ws_level_4_icon = ICONS[$weaponset_value['weapons']['3']];
        $ws_level_5_icon = ICONS[$weaponset_value['weapons']['4']];
        $ws_level_6_icon = ICONS[$weaponset_value['weapons']['5']];
        $ws_level_7_icon = ICONS[$weaponset_value['weapons']['6']];
        $ws_level_8_icon = ICONS[$weaponset_value['weapons']['7']];

        require 'resources/weaponset_template.php';

        // Create weaponset category directory
        if( !is_dir( strtolower("./weaponsets/{$category_key}") ) )
          mkdir( strtolower("./weaponsets/{$category_key}") );

        // Create weaponset page
        $weapon_set_page = fopen( strtolower("./weaponsets/{$category_key}/{$weaponset}.md"), "w");
        fwrite($weapon_set_page, $textData);

        // close weapon set page file
        fclose($weapon_set_page);

        // Write the table row to the summary page
        require 'resources/weaponset_summary_template.php';
        fwrite($weapon_set_summary_page, $summary_table_row);

        // Write the table row to the summary CSV
        fputcsv($weapon_set_summary_csv, $csv_row);

      } // end weapon set

    } // end category

  } // end file

  fclose($weapon_set_summary_page);
  fclose($weapon_set_summary_csv);

}

function main()
{

  global $argv;

  validateArgs();

  $loadoutPath = $argv[1];
  // Remove trailing slashes
  $loadoutPath = rtrim($loadoutPath,'/');
  $loadoutPath = rtrim($loadoutPath,'\\');

  generate($loadoutPath);

}

return(main());
