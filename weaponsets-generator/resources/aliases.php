<?php

// Weapon names
const NAMES = array(
  'PP7' => '[PP7](/weapons/pp7)',
  'PP7_SILENCED' => '[PP7 (Silenced)](/weapons/pp7_silenced)',
  'DD44' => '[DD44](/weapons/dd44)',
  'SILVER_PP7' => '[Silver PP7](/weapons/silver_pp7)',
  'GOLDEN_PP7' => '[Gold PP7](/weapons/gold_pp7)',
  'CMAG' => '[Cougar Magnum](/weapons/cougar_magnum)',
  'GOLDEN_GUN' => '[Golden Gun](/weapons/golden_gun)',
  'SHOTGUN' => '[Shotgun](/weapons/shotgun)',
  'AUTO_SHOTGUN' => '[Automatic Shotgun](/weapons/automatic_shotgun)',
  'KF7' => '[KF7 Soviet](/weapons/kf7_soviet)',
  'KLOBB' => '[Klobb](/weapons/klobb)',
  'ZMG' => '[ZMG](/weapons/zmg)',
  'D5K' => '[D5K Deutsche](/weapons/d5k_deutsche)',
  'D5K_SILENCED' => '[D5K (Silenced)](/weapons/d5k_silenced)',
  'RCP90' => '[RC-P90](/weapons/rc-p90)',
  'AR33' => '[AR33](/weapons/ar33)',
  'PHANTOM' => '[Phantom](/weapons/phantom)',
  'SNIPER_RIFLE' => '[Sniper Rifle](/weapons/sniper_rifle)',
  'KNIFE_THROWING' => '[Throwing Knife](/weapons/throwing_knife)',
  'TIMEDMINE' => '[Timed Mine](/weapons/timed_mine)',
  'REMOTEMINE' => '[Remote Mine](/weapons/remote_mine)',
  'PROXIMITYMINE' => '[Proximity Mine](/weapons/proximity_mine)',
  'GRENADE' => '[Grenade](/weapons/grenade)',
  'GRENADE_LAUNCHER' => '[Grenade Launcher](/weapons/grenade_launcher)',
  'ROCKET_LAUNCHER' => '[Rocket Launcher](/weapons/rocket_launcher)',
  'MOONRAKER' => '[Moonraker Laser](/weapons/moonraker_laser)',
  'KNIFE' => '[Hunting Knife](/weapons/hunting_knife)',
  'NONE' => 'None',
  'RANDOM' => 'Contextual Random Weapon',
  'RANDOM_EXPLOSIVE' => 'Random Explosive Weapon',
  'RANDOM_HITSCAN' => 'Random Bullet Weapon',
  'ULTRA_RANDOM' => 'Completely Random Weapon'
);

const ICONS = array(
  'PP7' => '![](/weapons/images/weaponset-icons/pp7.png?lightbox=false)',
  'PP7_SILENCED' => '![](/weapons/images/weaponset-icons/pp7_silenced.png?lightbox=false)',
  'DD44' => '![](/weapons/images/weaponset-icons/dd44.png?lightbox=false)',
  'SILVER_PP7' => '![](/weapons/images/weaponset-icons/silver_pp7.png?lightbox=false)',
  'GOLDEN_PP7' => '![](/weapons/images/weaponset-icons/golden_pp7.png?lightbox=false)',
  'CMAG' => '![](/weapons/images/weaponset-icons/cmag.png?lightbox=false)',
  'GOLDEN_GUN' => '![](/weapons/images/weaponset-icons/golden_gun.png?lightbox=false)',
  'SHOTGUN' => '![](/weapons/images/weaponset-icons/shotgun.png?lightbox=false)',
  'AUTO_SHOTGUN' => '![](/weapons/images/weaponset-icons/auto_shotgun.png?lightbox=false)',
  'KF7' => '![](/weapons/images/weaponset-icons/kf7.png?lightbox=false)',
  'KLOBB' => '![](/weapons/images/weaponset-icons/klobb.png?lightbox=false)',
  'ZMG' => '![](/weapons/images/weaponset-icons/zmg.png?lightbox=false)',
  'D5K' => '![](/weapons/images/weaponset-icons/d5k.png?lightbox=false)',
  'D5K_SILENCED' => '![](/weapons/images/weaponset-icons/d5k_silenced.png?lightbox=false)',
  'RCP90' => '![](/weapons/images/weaponset-icons/rcp90.png?lightbox=false)',
  'AR33' => '![](/weapons/images/weaponset-icons/ar33.png?lightbox=false)',
  'PHANTOM' => '![](/weapons/images/weaponset-icons/phantom.png?lightbox=false)',
  'SNIPER_RIFLE' => '![](/weapons/images/weaponset-icons/sniper_rifle.png?lightbox=false)',
  'KNIFE_THROWING' => '![](/weapons/images/weaponset-icons/knife_throwing.png?lightbox=false)',
  'TIMEDMINE' => '![](/weapons/images/weaponset-icons/timedmine.png?lightbox=false)',
  'REMOTEMINE' => '![](/weapons/images/weaponset-icons/remotemine.png?lightbox=false)',
  'PROXIMITYMINE' => '![](/weapons/images/weaponset-icons/proximitymine.png?lightbox=false)',
  'GRENADE' => '![](/weapons/images/weaponset-icons/grenade.png?lightbox=false)',
  'GRENADE_LAUNCHER' => '![](/weapons/images/weaponset-icons/grenade_launcher.png?lightbox=false)',
  'ROCKET_LAUNCHER' => '![](/weapons/images/weaponset-icons/rocket_launcher.png?lightbox=false)',
  'MOONRAKER' => '![](/weapons/images/weaponset-icons/moonraker.png?lightbox=false)',
  'KNIFE' => '![](/weapons/images/weaponset-icons/knife.png?lightbox=false)',
  'NONE' => 'None',
  'RANDOM' => '',
  'RANDOM_EXPLOSIVE' => '',
  'RANDOM_HITSCAN' => '',
  'ULTRA_RANDOM' => ''
);
