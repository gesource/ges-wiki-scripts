# GE:S Wiki Scripts

These are scripts that automate some of the processes of maintaining the wiki.

## Index:

* [weaponsets-generator](weaponsets-generator): Automatically generates weaponset pages from the game's weaponset scripts.
